package first.mymapzz.com.miniproject1.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServicGenerator {

    public static Retrofit retrofit = null;
    private final static String BASE_ACTICLE_URL = "http://www.api-ams.me/";
    public static Retrofit getInstanceRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_ACTICLE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;
        }
        return retrofit;
    }
}
