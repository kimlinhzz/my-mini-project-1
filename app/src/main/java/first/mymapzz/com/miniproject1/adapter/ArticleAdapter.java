package first.mymapzz.com.miniproject1.adapter;

import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import first.mymapzz.com.miniproject1.R;
import first.mymapzz.com.miniproject1.entities.Article;
import first.mymapzz.com.miniproject1.interfaces.OnItemRecyclerClickListener;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolde> {
    public static final String DATE_FORMAT_2 = "yyyy-mm-dd-hh-mm-ss";
    List<Article> articles;
    OnItemRecyclerClickListener onItemRecyclerClickListener;

    public ArticleAdapter(List<Article> articles, OnItemRecyclerClickListener onItemRecyclerClickListener) {
        this.articles = articles;
        this.onItemRecyclerClickListener = onItemRecyclerClickListener;
    }

    @NonNull
    @Override
    public ArticleViewHolde onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.article_list, parent, false);
        return new ArticleViewHolde(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolde holder, int position) {
        Article article = articles.get(position);
        Glide.with(holder.imageView.getContext())
                .load(article.getImageURL())
                .placeholder(R.drawable.default_image)
                .into(holder.imageView);
        holder.textTitle.setText(article.getTitle());
        holder.textDesc.setText(article.getDesc());
        try {
            Log.d("TAG!@#","Trying");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(article.getCreateDate()));
            Log.d("TAG!@#", "onBindViewHolder: "+ localDateTime);
            holder.textDate.setText(localDateTime.toString());
        }catch (Exception e){
            Log.d("TAG!@#","bla bla"+e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        if (articles != null)
            return articles.size();
        return 0;
    }


    class ArticleViewHolde extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textTitle;
        TextView textDate;
        TextView textDesc;
        ImageView imageView, imageMore;

        public ArticleViewHolde(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_cover);
            textDate = itemView.findViewById(R.id.text_date);
            textTitle = itemView.findViewById(R.id.text_title);
            textDesc = itemView.findViewById(R.id.text_desc);
            imageMore = itemView.findViewById(R.id.btn_more);
            itemView.setOnClickListener(this);
            imageMore.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Article article = articles.get(getAdapterPosition());
            if (view == itemView) {
                onItemRecyclerClickListener.onRootItemRecyclerClick(view, getAdapterPosition(), article.getId());
            }
            if (view == imageMore) {
                onItemRecyclerClickListener.onChildItemRecyclerClick(view, getAdapterPosition(), article.getId());
            }
        }
    }
}
