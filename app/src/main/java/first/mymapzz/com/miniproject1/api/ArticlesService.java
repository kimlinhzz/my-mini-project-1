package first.mymapzz.com.miniproject1.api;

import first.mymapzz.com.miniproject1.entities.Article;
import first.mymapzz.com.miniproject1.entities.ImageResult;
import first.mymapzz.com.miniproject1.entities.RespondeArticle;
import first.mymapzz.com.miniproject1.entities.RespondeArticles;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticlesService {

    @GET("v1/api/articles")
    Call<RespondeArticles> getResponseArticle(@Query("page") int page, @Query("limit") int limit);

    @GET("v1/api/articles/{id}")
    Call<RespondeArticle> getResponseArticleByID(@Path("id") long id);

    @POST("v1/api/articles")
    Call<ResponseBody> postArticle(@Body Article article);

    @Multipart
    @POST("v1/api/uploadfile/single")
    Call<ImageResult> uploadImage(@Part MultipartBody.Part file);

    @DELETE("v1/api/articles/{id}")
    Call<ResponseBody> deleteArticleById(@Path("id") int id);

    @PUT("v1/api/articles/{id}")
    Call<ResponseBody> updateArticleById(@Path("id") int id , @Body Article article);
}
