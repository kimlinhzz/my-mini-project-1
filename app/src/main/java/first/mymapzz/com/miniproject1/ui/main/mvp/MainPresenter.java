package first.mymapzz.com.miniproject1.ui.main.mvp;

import java.util.List;

import first.mymapzz.com.miniproject1.entities.Article;


public class MainPresenter implements MainInterface.Presenter {

    private MainInteractor mainInteracter;
    private MainInterface.View view;

    public MainPresenter() {
        mainInteracter = new MainInteractor();
    }

    @Override
    public void onRefreshArticle() {
        view.showProcessBar();
        mainInteracter.getArticleFromServer(1, 15, new MainInterface.Interactor.OnCallBack() {
            @Override
            public void onSuccess(List<Article> articles) {
                view.hideSwipeRefresher();
                view.hideProcessBar();
                view.onResponseToRecyclerView(articles);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideSwipeRefresher();
                view.hideProcessBar();
                view.onResponseFailure(throwable);
            }
        });
    }

    //Get Default Data for recyclerview
    @Override
    public void onRequestArticle() {
        view.showProcessBar();
        mainInteracter.getArticleFromServer(1, 15, new MainInterface.Interactor.OnCallBack() {
            @Override
            public void onSuccess(List<Article> articles) {
                view.onResponseToRecyclerView(articles);
                view.hideProcessBar();
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.onResponseFailure(throwable);
                view.hideProcessBar();
            }
        });

    }

    @Override
    public void setView(MainInterface.View view) {
        this.view = view;
    }

    @Override
    public void getMoreArticle(int page, int limit) {
        view.showProcessBar();
        mainInteracter.getArticleFromServer(page, limit, new MainInterface.Interactor.OnCallBack() {
            @Override
            public void onSuccess(List<Article> articles) {
                view.hideProcessBar();
                view.onResponseToRecyclerView(articles);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideProcessBar();
                view.onResponseFailure(throwable);
            }
        });
    }

    @Override
    public void deleteArticleById(int id) {
        view.showProcessBar();
        mainInteracter.deleteArticleFromServer(id, new MainInterface.Interactor.OnDeleteCallBack() {
            @Override
            public void onSuccess(String message) {
                view.hideProcessBar();
                    view.onDeleteArticleSuccess(message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideProcessBar();
                view.onDeleteArticleFail(throwable);
            }
        });
    }
}
