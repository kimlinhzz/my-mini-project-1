package first.mymapzz.com.miniproject1.ui.detail;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import first.mymapzz.com.miniproject1.R;
import first.mymapzz.com.miniproject1.entities.Article;
import first.mymapzz.com.miniproject1.ui.detail.mvp.DetailInterface;
import first.mymapzz.com.miniproject1.ui.detail.mvp.DetailPresenter;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DetailActivity extends AppCompatActivity implements DetailInterface.View {

    DetailPresenter detailPresenter;
    int articleId;
    private ProgressBar progressBar;
    private ImageView imageCover;
    private TextView textTitle, textCreateDate, textDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        registerView();
        Intent intent = getIntent();
        articleId = intent.getIntExtra("article-id", 0);
        detailPresenter = new DetailPresenter();
        detailPresenter.setView(this);
        if (articleId != 0) {
            Log.d("ID!@#", "onCreate: " + articleId);
            detailPresenter.requestArticleById(articleId);
        } else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }

    }

    private void registerView() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        textCreateDate = findViewById(R.id.text_create_date_value);
        textDesc = findViewById(R.id.text_desc_value);
        textTitle = findViewById(R.id.text_title_value);
        imageCover = findViewById(R.id.image_cover);
        progressBar = findViewById(R.id.progress_bar_detail);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return true;
    }

    @Override
    public void showProcessBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProcessBar() {
        progressBar.setVisibility(View.GONE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void getArticleSuccess(Article article) {
        if (article != null) {
            textTitle.setText(article.getTitle());
            textDesc.setText(article.getDesc());
            textCreateDate.setText(getDateFormat(article.getCreateDate()));
            Glide.with(imageCover.getContext())
                    .load(article.getImageURL())
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .into(imageCover);
        } else {
            Toast.makeText(this, "Article is null right now", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getArticleFail(Throwable throwable) {
        Toast.makeText(this, "Something went wrong...", Toast.LENGTH_SHORT).show();
        Log.d("ERROR!@#", "getArticleFail: " + throwable.toString());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getDateFormat(String date){
        try {
            Log.d("TAG!@#","Trying");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(date));
            Log.d("TAG!@#", "onBindViewHolder: "+ localDateTime);
            return localDateTime.toString();
        }catch (Exception e){
            Log.d("TAG!@#","bla bla"+e.getMessage());
            return null;
        }
    }

}
