package first.mymapzz.com.miniproject1.ui.input.mvp;

import android.content.Context;
import android.net.Uri;

import first.mymapzz.com.miniproject1.entities.Article;

public interface InputInterface {

    interface View {
        void onResponseSuccessUploadImage(String Message);

        void onResponseFailAdd(Throwable throwable);

        void showProcessBar();

        void hideProcessBar();

        void showArticleById(Article article);

        void updateArticleSuccess(String message);

        void updateArticleFail(String message);
    }

    interface Presenter {
        void onUpdateImage(int id, Article article, Uri imageUri, Context context);

        void onGetSelectedArticle(int id);

        void onAddImage(String title, String desc, Uri imageUri, Context context);

        void setView(View view);
    }

    interface Interactor {
        //Upload Image
        interface UploadImageCallBack {
            void onSuccessUploadImage(String imageUrl);

            void onFailUploadImage(Throwable throwable);
        }

        void onImageAddData(Uri uriImage, Context context, UploadImageCallBack uploadImageCallBack);

        //Update Article
        interface UpdateArticleCallBack {
            void onSuccessUpdateArticle(String message);

            void onSFailUpdateArticle(String message);
        }

        void onUpdateArticle(int id, Article article, UpdateArticleCallBack updateArticleCallBack);

        //Get Article
        interface GetSelectedArticleCallBack {
            void onSuccessGetArticle(Article article);

            void onFailGetArticle(String message);
        }

        void onGetArticleById(int id, GetSelectedArticleCallBack getSelectedArticleCallBack);

        //Upload Article
        interface UploadArticleCallBack {
            void onSuccessUploadImage(String Message);

            void onFailUploadImage(Throwable throwable);
        }

        void onArticleUpload(Article article, UploadArticleCallBack uploadArticleCallBack);
    }
}
