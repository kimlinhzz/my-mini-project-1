package first.mymapzz.com.miniproject1.interfaces;

import android.view.View;

public interface OnItemRecyclerClickListener {
    void onRootItemRecyclerClick(View view, int position , int id);
    void onChildItemRecyclerClick(View view, int position, int id);
}
