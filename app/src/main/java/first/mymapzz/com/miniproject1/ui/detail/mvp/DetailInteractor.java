package first.mymapzz.com.miniproject1.ui.detail.mvp;

import android.util.Log;

import first.mymapzz.com.miniproject1.api.ArticlesService;
import first.mymapzz.com.miniproject1.api.ServicGenerator;
import first.mymapzz.com.miniproject1.entities.Article;
import first.mymapzz.com.miniproject1.entities.RespondeArticle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailInteractor implements DetailInterface.Interactor {
    @Override
    public void getArticleFromServer(int id, final GetArticleCallBack getArticleCallBack) {
        ArticlesService articlesService = ServicGenerator.getInstanceRetrofit().create(ArticlesService.class);
        Call<RespondeArticle> call = articlesService.getResponseArticleByID(id);
        call.enqueue(new Callback<RespondeArticle>() {
            @Override
            public void onResponse(Call<RespondeArticle> call, Response<RespondeArticle> response) {
                Article article = response.body().getArticles();
                getArticleCallBack.onGetArticleSuccess(article);
                Log.d("DATA!@#", "onFailure: " + response.body().getMessage());
            }

            @Override
            public void onFailure(Call<RespondeArticle> call, Throwable t) {
                getArticleCallBack.onGetArticleFail(t);
                Log.d("DATA!@#", "onFailure: " + t.toString());
            }
        });
    }
}