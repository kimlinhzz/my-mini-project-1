package first.mymapzz.com.miniproject1.ui.main.mvp;

import android.util.Log;

import java.util.List;

import first.mymapzz.com.miniproject1.entities.Article;
import first.mymapzz.com.miniproject1.entities.RespondeArticles;
import first.mymapzz.com.miniproject1.api.ArticlesService;
import first.mymapzz.com.miniproject1.api.ServicGenerator;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainInteractor implements MainInterface.Interactor {

    @Override
    public void deleteArticleFromServer(int id, final OnDeleteCallBack onDeleteCallBack) {
        ArticlesService articlesService = ServicGenerator.getInstanceRetrofit().create(ArticlesService.class);
        Call<ResponseBody> call = articlesService.deleteArticleById(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                onDeleteCallBack.onSuccess("Successfully Delete");
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onDeleteCallBack.onFailure(t);
            }
        });
    }

    @Override
    public void getArticleFromServer(int page, int limit, final OnCallBack onCallBack) {
        ArticlesService articlesService = ServicGenerator.getInstanceRetrofit()
                .create(ArticlesService.class);

        final Call<RespondeArticles> responseArticleCall = articlesService.getResponseArticle(page, limit);
        responseArticleCall.enqueue(new Callback<RespondeArticles>() {
            @Override
            public void onResponse(Call<RespondeArticles> call, Response<RespondeArticles> response) {
                Log.d("DATA!@#", "" + response.message());
                List<Article> articleList = response.body().getArticles();
                onCallBack.onSuccess(articleList);
            }

            @Override
            public void onFailure(Call<RespondeArticles> call, Throwable t) {
                onCallBack.onFailure(t);
            }
        });


    }
}
