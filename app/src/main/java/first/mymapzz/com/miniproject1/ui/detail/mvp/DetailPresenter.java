package first.mymapzz.com.miniproject1.ui.detail.mvp;

import first.mymapzz.com.miniproject1.entities.Article;

public class DetailPresenter implements DetailInterface.Presenter {

    DetailInterface.View view;
    DetailInteractor detailInteractor;

    public DetailPresenter() {
        this.detailInteractor = new DetailInteractor();
    }

    @Override
    public void setView(DetailInterface.View view) {
        this.view = view;
    }

    @Override
    public void requestArticleById(int id) {
        view.showProcessBar();
        detailInteractor.getArticleFromServer(id, new DetailInterface.Interactor.GetArticleCallBack() {
            @Override
            public void onGetArticleSuccess(Article article) {
                view.getArticleSuccess(article);
                view.hideProcessBar();
            }

            @Override
            public void onGetArticleFail(Throwable throwable) {
                view.hideProcessBar();
                view.getArticleFail(throwable);
            }
        });
    }
}
