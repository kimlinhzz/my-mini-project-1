package first.mymapzz.com.miniproject1.ui.input.mvp;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import first.mymapzz.com.miniproject1.entities.Article;

public class InputPresenter implements InputInterface.Presenter {

    private InputInteractor inputInteracter;
    private InputInterface.View view;

    public InputPresenter() {
        inputInteracter = new InputInteractor();
    }

    @Override
    public void onUpdateImage(final int id, final Article article, Uri imageUri, Context context) {
        view.showProcessBar();
        final Article tempAritcle = new Article();
        if (imageUri != null) {
            inputInteracter.onImageAddData(imageUri, context, new InputInterface.Interactor.UploadImageCallBack() {
                @Override
                public void onSuccessUploadImage(String imageUrl) {

                    tempAritcle.setTitle(article.getTitle());
                    tempAritcle.setDesc(article.getTitle());
                    tempAritcle.setImageURL(imageUrl);

                    inputInteracter.onUpdateArticle(id, tempAritcle, new InputInterface.Interactor.UpdateArticleCallBack() {
                        @Override
                        public void onSuccessUpdateArticle(String message) {
                            view.hideProcessBar();
                            view.updateArticleSuccess(message);
                        }

                        @Override
                        public void onSFailUpdateArticle(String message) {
                            view.hideProcessBar();
                            view.updateArticleFail(message);
                        }
                    });
                }

                @Override
                public void onFailUploadImage(Throwable throwable) {
                    view.hideProcessBar();
                }
            });
        } else {
            tempAritcle.setTitle(article.getTitle());
            tempAritcle.setDesc(article.getDesc());
            tempAritcle.setImageURL(article.getImageURL());
            inputInteracter.onUpdateArticle(id, tempAritcle, new InputInterface.Interactor.UpdateArticleCallBack() {
                @Override
                public void onSuccessUpdateArticle(String message) {
                    view.hideProcessBar();
                    view.updateArticleSuccess(message);
                }

                @Override
                public void onSFailUpdateArticle(String message) {
                    view.hideProcessBar();
                    view.updateArticleFail(message);
                }
            });
        }

    }

    @Override
    public void onGetSelectedArticle(int id) {
        view.showProcessBar();
        inputInteracter.onGetArticleById(id, new InputInterface.Interactor.GetSelectedArticleCallBack() {
            @Override
            public void onSuccessGetArticle(Article article) {
                view.hideProcessBar();
                view.showArticleById(article);
            }

            @Override
            public void onFailGetArticle(String message) {
                view.hideProcessBar();
                view.showArticleById(null);
            }
        });
    }

    @Override
    public void onAddImage(final String title, final String desc, Uri imageUri, Context context) {
        view.showProcessBar();
        final Article article = new Article();
        if (imageUri != null) {
            inputInteracter.onImageAddData(imageUri, context, new InputInterface.Interactor.UploadImageCallBack() {
                @Override
                public void onSuccessUploadImage(String imageUrl) {
                    view.hideProcessBar();
                    article.setDesc(desc);
                    article.setTitle(title);
                    article.setImageURL(imageUrl);
                    inputInteracter.onArticleUpload(article, new InputInterface.Interactor.UploadArticleCallBack() {
                        @Override
                        public void onSuccessUploadImage(String Message) {
                            view.onResponseSuccessUploadImage("hello" + Message);
                        }

                        @Override
                        public void onFailUploadImage(Throwable throwable) {
                            view.onResponseFailAdd(throwable);
                        }
                    });
                }

                @Override
                public void onFailUploadImage(Throwable throwable) {
                    view.hideProcessBar();
//                view.onResponseFailAdd(throwable);
                }
            });
        } else {
            article.setDesc(desc);
            article.setTitle(title);
            inputInteracter.onArticleUpload(article, new InputInterface.Interactor.UploadArticleCallBack() {
                @Override
                public void onSuccessUploadImage(String Message) {
                    view.onResponseSuccessUploadImage("hello" + Message);
                }

                @Override
                public void onFailUploadImage(Throwable throwable) {
                    view.onResponseFailAdd(throwable);
                }
            });
        }

    }

    @Override
    public void setView(InputInterface.View view) {
        this.view = view;
    }
}
