package first.mymapzz.com.miniproject1.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespondeArticles {
    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private List<Article> articles;
    @SerializedName("IMAGE")
    private String imageUrl;

    public RespondeArticles(String code, String message, List<Article> articles, String imageUrl) {
        this.code = code;
        this.message = message;
        this.articles = articles;
        this.imageUrl = imageUrl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
