package first.mymapzz.com.miniproject1.entities;

import android.widget.ImageView;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Article {

    @SerializedName("ID")
    private int id;
    @SerializedName("TITLE")
    private String title;
    @SerializedName("DESCRIPTION")
    private String desc;
    @SerializedName("CREATED_DATE")
    private String createDate;
    @SerializedName("IMAGE")
    private String imageURL;

    public Article(){

    }
    public Article(String title, String desc, String createDate, String imageURL) {
        this.title = title;
        this.desc = desc;
        this.createDate = createDate;
        this.imageURL = imageURL;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", createDate='" + createDate + '\'' +
                ", imageURL='" + imageURL + '\'' +
                '}';
    }
}
