package first.mymapzz.com.miniproject1.ui.input.mvp;

import android.content.Context;
import android.net.Uri;
import android.os.FileUtils;
import android.util.Log;

import java.io.File;

import first.mymapzz.com.miniproject1.entities.Article;
import first.mymapzz.com.miniproject1.entities.ImageResult;
import first.mymapzz.com.miniproject1.api.ArticlesService;
import first.mymapzz.com.miniproject1.api.ServicGenerator;
import first.mymapzz.com.miniproject1.entities.RespondeArticle;
import first.mymapzz.com.miniproject1.utils.FileUtiles;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputInteractor implements InputInterface.Interactor {

    @Override
    public void onImageAddData(Uri uriImage, Context context, final UploadImageCallBack uploadImageCallBack) {
        ArticlesService articlesService = ServicGenerator.getInstanceRetrofit().create(ArticlesService.class);
        Log.d("TAG", "onImageAddData: " + uriImage);
        File file = new File(FileUtiles.getPath(uriImage, context));
        RequestBody fileRequestBody = RequestBody.create(MediaType.parse("form-data"), file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("FILE", file.getName(), fileRequestBody);
        Call<ImageResult> call = articlesService.uploadImage(part);

        call.enqueue(new Callback<ImageResult>() {
            @Override
            public void onResponse(Call<ImageResult> call, Response<ImageResult> response) {
                ImageResult imageResult = response.body();
                Log.d("DATAIMAGE!@#", "getData :" + imageResult.getData());
                uploadImageCallBack.onSuccessUploadImage(imageResult.getData());
            }

            @Override
            public void onFailure(Call<ImageResult> call, Throwable t) {
                Log.d("DATAIMAGE!@#", t.toString());
                uploadImageCallBack.onFailUploadImage(t);
            }
        });
    }


    @Override
    public void onUpdateArticle(int id, Article newArticle, final UpdateArticleCallBack updateArticleCallBack) {
        ArticlesService articlesService = ServicGenerator.getInstanceRetrofit().create(ArticlesService.class);
        Call<ResponseBody> call = articlesService.updateArticleById(id, newArticle);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                updateArticleCallBack.onSuccessUpdateArticle("Success" + response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                updateArticleCallBack.onSFailUpdateArticle("Error" + t);
            }
        });

    }

    @Override
    public void onGetArticleById(int id, final GetSelectedArticleCallBack getSelectedArticleCallBack) {
        ArticlesService articlesService = ServicGenerator.getInstanceRetrofit().create(ArticlesService.class);
        Call<RespondeArticle> call = articlesService.getResponseArticleByID(id);
        call.enqueue(new Callback<RespondeArticle>() {
            @Override
            public void onResponse(Call<RespondeArticle> call, Response<RespondeArticle> response) {
                Article article = response.body().getArticles();
                getSelectedArticleCallBack.onSuccessGetArticle(article);
            }

            @Override
            public void onFailure(Call<RespondeArticle> call, Throwable t) {
                getSelectedArticleCallBack.onFailGetArticle("Error" + t.toString());
            }
        });
    }

    @Override
    public void onArticleUpload(Article article, final UploadArticleCallBack uploadArticleCallBack) {
        ArticlesService articlesService = ServicGenerator.getInstanceRetrofit().create(ArticlesService.class);
        Call<ResponseBody> call = articlesService.postArticle(article);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                uploadArticleCallBack.onSuccessUploadImage("Input Success");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                uploadArticleCallBack.onFailUploadImage(t);
            }
        });
    }
}
