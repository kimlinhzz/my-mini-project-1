package first.mymapzz.com.miniproject1.ui.main.mvp;

import java.util.List;

import first.mymapzz.com.miniproject1.entities.Article;

public interface MainInterface {

    interface View {


        void showSwipeRefresher();

        void hideSwipeRefresher();

        void showProcessBar();

        void hideProcessBar();

        void onResponseFailure(Throwable throwable);

        void onResponseToRecyclerView(List<Article> getAllArticles);

        void onDeleteArticleSuccess(String message);

        void onDeleteArticleFail(Throwable throwable);
    }

    interface Presenter {
        void onRefreshArticle();

        void onRequestArticle();

        void setView(View view);

        void getMoreArticle(int page, int limit);

        void deleteArticleById(int id);
    }

    interface Interactor {
        interface OnCallBack {
            void onSuccess(List<Article> articles);

            void onFailure(Throwable throwable);
        }

        interface OnDeleteCallBack {
            void onSuccess(String message);

            void onFailure(Throwable throwable);
        }

        void deleteArticleFromServer(int id, OnDeleteCallBack onDeleteCallBack);

        void getArticleFromServer(int page, int limit, OnCallBack onCallBack);
    }
}
