package first.mymapzz.com.miniproject1.ui.input;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import first.mymapzz.com.miniproject1.R;
import first.mymapzz.com.miniproject1.entities.Article;
import first.mymapzz.com.miniproject1.ui.input.mvp.InputInterface;
import first.mymapzz.com.miniproject1.ui.input.mvp.InputPresenter;
import first.mymapzz.com.miniproject1.ui.main.MainActivity;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;


public class InputActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks, InputInterface.View {

    private static final int REQUEST_CODE_FOR_GALLERY = 1211;
    private static final int REQUEST_CODE_FOR_PERMISSION_GALLERY = 1111;

    private Button btnCancel, btnSave;
    private EditText editTitle, editDesc;
    private ImageView imageCover;
    private Uri imageTempUri;
    private InputPresenter inputPresenter;
    private ProgressBar progressBar;
    int article_id;
    private boolean isUpdate = false;
    private Article currentArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        registerView();
        progressBar.setVisibility(View.GONE);
        inputPresenter = new InputPresenter();
        inputPresenter.setView(this);
        if (getIntent() != null) {
            article_id = getIntent().getIntExtra("article_id", 0);
            inputPresenter.onGetSelectedArticle(article_id);
        }
    }

    void registerView() {
        currentArticle = new Article();
        btnCancel = findViewById(R.id.btn_cancel);
        btnSave = findViewById(R.id.btn_save);
        editTitle = findViewById(R.id.ed_title);
        editDesc = findViewById(R.id.ed_desc);
        imageCover = findViewById(R.id.image_cover);
        progressBar = findViewById(R.id.progress_bar_detail);
        imageCover.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String title = editTitle.getText().toString();
        String desc = editDesc.getText().toString();
        if (view == btnCancel) {
            finish();
        }
        if (view == imageCover) {
            openGallery();
        }
        if (view == btnSave) {
            if (!isUpdate) {
                if (isNotNull(title, desc)) {
                    inputPresenter.onAddImage(editTitle.getText().toString(), editDesc.getText().toString(), imageTempUri, this);
                }else {
                    Toast.makeText(InputActivity.this , "Please input all the information",Toast.LENGTH_SHORT).show();
                }
            }
            if (isUpdate) {
                if (currentArticle != null) {
                    if (isNotNull(title, desc)) {
                        currentArticle.setDesc(desc);
                        currentArticle.setTitle(title);
                        Toast.makeText(InputActivity.this, "Good to go", Toast.LENGTH_SHORT).show();
                        inputPresenter.onUpdateImage(currentArticle.getId(), currentArticle, imageTempUri, this);
                    } else {
                        Toast.makeText(InputActivity.this, "Please input all the information", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private boolean isNotNull(String title, String desc) {
        if (title.length() == 0 && title.isEmpty() || desc.length() == 0 && desc.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    @AfterPermissionGranted(REQUEST_CODE_FOR_PERMISSION_GALLERY)
    void openGallery() {
        String[] prms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(InputActivity.this, prms)) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, REQUEST_CODE_FOR_GALLERY);
            Toast.makeText(InputActivity.this, "Good to go", Toast.LENGTH_SHORT).show();
        } else {
            EasyPermissions.requestPermissions(InputActivity.this, "We need this permission for accessing gallery", REQUEST_CODE_FOR_PERMISSION_GALLERY, prms);
            Toast.makeText(InputActivity.this, "Requesting", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_FOR_GALLERY) {
            Uri imageUri = data.getData();
            Glide.with(imageCover.getContext()).load(imageUri).into(imageCover);
            if (imageUri != null) {
                imageTempUri = imageUri;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(REQUEST_CODE_FOR_PERMISSION_GALLERY, permissions, grantResults, InputActivity.this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(InputActivity.this, perms)) {
            new AppSettingsDialog.Builder(InputActivity.this).build().show();
        }
    }

    @Override
    public void onResponseSuccessUploadImage(String Message) {
        Toast.makeText(this, "Success" + Message, Toast.LENGTH_SHORT).show();
        Log.d("ERRORImage", Message);
        finish();
    }

    @Override
    public void onResponseFailAdd(Throwable throwable) {
        Log.d("ERRORADD", throwable.toString());
    }

    @Override
    public void showProcessBar() {
        progressBar.bringToFront();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProcessBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showArticleById(Article article) {
        if (article != null) {
            currentArticle = article;
            isUpdate = true;
            btnSave.setText("Update");
            Glide.with(this)
                    .load(article.getImageURL())
                    .placeholder(R.drawable.default_image)
                    .into(imageCover);
            editDesc.setText(article.getDesc());
            editTitle.setText(article.getTitle());
            Toast.makeText(this, "Update Mode", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Insert Mode", Toast.LENGTH_SHORT).show();
            isUpdate = false;
            btnSave.setText("Add");
        }
    }

    @Override
    public void updateArticleSuccess(String message) {
        Log.d("SUCCESS!@#", message);
        Toast.makeText(this, "Update Complete!!", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void updateArticleFail(String message) {
        Log.d("ERROR!@#", message);
        Toast.makeText(this, "Something went wrong...", Toast.LENGTH_SHORT).show();
    }
}
