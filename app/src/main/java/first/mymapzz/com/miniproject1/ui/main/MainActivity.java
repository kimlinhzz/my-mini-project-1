package first.mymapzz.com.miniproject1.ui.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import first.mymapzz.com.miniproject1.R;
import first.mymapzz.com.miniproject1.adapter.ArticleAdapter;
import first.mymapzz.com.miniproject1.entities.Article;
import first.mymapzz.com.miniproject1.interfaces.OnItemRecyclerClickListener;
import first.mymapzz.com.miniproject1.ui.detail.DetailActivity;
import first.mymapzz.com.miniproject1.ui.input.InputActivity;
import first.mymapzz.com.miniproject1.ui.main.mvp.MainInterface;
import first.mymapzz.com.miniproject1.ui.main.mvp.MainPresenter;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainInterface.View, OnItemRecyclerClickListener
        , SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, PopupMenu.OnMenuItemClickListener {

    private MainPresenter mainPresenter;
    private ProgressBar progressBar;
    private RecyclerView rcyArticle;
    private SwipeRefreshLayout swiptRefreshData;
    private FloatingActionButton fabAdd;
    ArticleAdapter articleAdapter;
    List<Article> listArticles;
    LinearLayoutManager layoutManager;
    PopupMenu popupMenu;
    //handle event in recycler to load more data
    private boolean isLoading = true;
    private int pages = 1;
    private int limit = 15;
    private int totalItemCount = 0;
    private int currenteItemCount = 0;
    private int previousTotalItem = 0;
    private int countUnseenItems = 0;
    private int articleTempID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerView();
        progressBar.setVisibility(View.GONE);
        mainPresenter = new MainPresenter();
        mainPresenter.setView(this);
        mainPresenter.onRequestArticle();
        fabAdd.setOnClickListener(this);
        articleAdapter = new ArticleAdapter(this.listArticles, this);
        rcyArticle.setAdapter(articleAdapter);
        rcyArticle.setLayoutManager(layoutManager = new LinearLayoutManager(this));
        swiptRefreshData.setOnRefreshListener(this);
        getMoreArticle();
    }

    void getMoreArticle() {

        rcyArticle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = layoutManager.getItemCount();
                currenteItemCount = recyclerView.getChildCount();
                countUnseenItems = layoutManager.findFirstVisibleItemPosition();

                if (isLoading) {
                    if (totalItemCount > previousTotalItem) {
                        previousTotalItem = totalItemCount;
                        isLoading = false;
                        Log.d("REYCLEVIEW!@#", "isLoading:" + isLoading);
                    }
                }
                Log.d("REYCLEVIEW!@#", "cal:" + (totalItemCount - countUnseenItems));
                Log.d("REYCLEVIEW!@#", "cal:" + (totalItemCount - countUnseenItems));
                int i = 0;
                if (!isLoading && (totalItemCount - currenteItemCount) <= (countUnseenItems + 5)) {
                    Log.d("REYCLEVIEW!@#", "Loading More at :" + countUnseenItems + 5);
                    Log.d("REYCLEVIEW!@#", "pages:" + pages + "limit:" + limit);
                    Log.d("REYCLEVIEW!@#", "isLoading:" + totalItemCount + "current-visible" + currenteItemCount);
                    Log.d("REYCLEVIEW!@#", "Count getMore:" + i);
                    mainPresenter.getMoreArticle(pages, limit);
                    Toast.makeText(MainActivity.this, "Loading more data...", Toast.LENGTH_SHORT).show();
                    isLoading = true;
                    return;
                }
                // Hide and show Filter button
                if (dy > 0 && fabAdd.getVisibility() == View.VISIBLE) {
                    fabAdd.hide();
                } else if (dy < 0 && fabAdd.getVisibility() != View.VISIBLE) {
                    fabAdd.show();
                }
            }
        });
    }

    void registerView() {
        listArticles = new ArrayList<>();
        fabAdd = findViewById(R.id.fab_add);
        swiptRefreshData = findViewById(R.id.swiper_refresh_data);
        rcyArticle = findViewById(R.id.rcy_article);
        progressBar = findViewById(R.id.progress_bar_detail);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        listArticles.clear();
        mainPresenter.onRequestArticle();
        resetPagination();
    }

    @Override
    public void showProcessBar() {
        progressBar.bringToFront();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSwipeRefresher() {
        swiptRefreshData.setRefreshing(true);
    }

    @Override
    public void hideSwipeRefresher() {
        swiptRefreshData.setRefreshing(false);
    }

    @Override
    public void hideProcessBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        progressBar.setVisibility(View.GONE);
        Log.d("ERORR!@#", throwable.toString());
        Toast.makeText(this, "Error hai bb", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponseToRecyclerView(List<Article> getAllArticles) {
        Log.d("ORIGINAL!@#", "SIZE" + getAllArticles.size());
        listArticles.addAll(getAllArticles);
        this.articleAdapter.notifyDataSetChanged();
        Log.d("LIST!@#", "CurrentSize" + listArticles.size());
        pages++;
    }

    @Override
    public void onDeleteArticleSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        listArticles.clear();
        mainPresenter.onRequestArticle();
        resetPagination();
    }

    @Override
    public void onDeleteArticleFail(Throwable throwable) {
        Log.d("DELETE!@#", throwable.toString());
        Toast.makeText(this, "Error :" + throwable.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRootItemRecyclerClick(View view, int position, int id) {

        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra("article-id", id);
        Log.d("ID!@#", "onRootItemRecyclerClick: " + id);
        startActivity(intent);
    }

    @Override
    public void onChildItemRecyclerClick(View view, int position, int id) {

        articleTempID = id;
        popupMenu = new PopupMenu(MainActivity.this, view);
        popupMenu.inflate(R.menu.option_menu);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    @Override
    public void onRefresh() {
        listArticles.clear();
        mainPresenter.onRefreshArticle();
        resetPagination();
    }

    @Override
    public void onClick(View view) {
        if (view == fabAdd) {
            Intent intent = new Intent(MainActivity.this, InputActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.edit:
                Intent intent = new Intent(MainActivity.this, InputActivity.class);
                if (articleTempID != 0) {
                    intent.putExtra("article_id", articleTempID);
                    startActivity(intent);
                }
                return true;
            case R.id.delete:
                deleteArticle();
                return true;
            case R.id.cancel:
                popupMenu.dismiss();
                return true;
        }
        return false;
    }

    private void deleteArticle() {
        new AlertDialog.Builder(this)
                .setTitle("Delete Article ?")
                .setMessage("Are sure you want to delete this article")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mainPresenter.deleteArticleById(articleTempID);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private void resetPagination() {
        pages = 1;
        totalItemCount = 0;
        currenteItemCount = 0;
        countUnseenItems = 0;
        previousTotalItem = 0;
        isLoading = true;
        this.articleAdapter.notifyDataSetChanged();
    }
}
