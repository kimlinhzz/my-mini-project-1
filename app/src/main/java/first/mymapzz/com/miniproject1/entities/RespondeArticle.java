package first.mymapzz.com.miniproject1.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RespondeArticle {
    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private Article articles;
    @SerializedName("IMAGE")
    private String imageUrl;

    public RespondeArticle(String code, String message, Article article, String imageUrl) {
        this.code = code;
        this.message = message;
        this.articles = article;
        this.imageUrl = imageUrl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Article getArticles() {
        return articles;
    }

    public void setArticles(Article articles) {
        this.articles = articles;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
