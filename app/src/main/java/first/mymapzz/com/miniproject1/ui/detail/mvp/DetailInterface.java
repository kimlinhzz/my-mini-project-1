package first.mymapzz.com.miniproject1.ui.detail.mvp;

import first.mymapzz.com.miniproject1.entities.Article;

public interface DetailInterface {

    interface View {
        void showProcessBar();

        void hideProcessBar();

        void getArticleSuccess(Article article);

        void getArticleFail(Throwable throwable);
    }

    interface Presenter {
        void setView(View view);

        void requestArticleById(int id);
    }

    interface Interactor {
        interface GetArticleCallBack {
            void onGetArticleSuccess(Article article);

            void onGetArticleFail(Throwable throwable);
        }

        void getArticleFromServer(int id, GetArticleCallBack getArticleCallBack);
    }
}
